{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./packages.nix
      ./networking.nix
    ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "pl";
    defaultLocale = "en_US.UTF-8";
  };

  programs = {
    zsh = {
      enable = true;
      autosuggestions.enable = true;
      syntaxHighlighting.enable = true;
      
      ohMyZsh = {
        enable = true;
      	theme = "norm";
      };

    };
  };

  time.timeZone = "Europe/Warsaw";
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services = {
    xserver = {
      enable = true;
      layout = "pl";
      libinput.enable = true;

      displayManager.sddm = {
        enable = true;
        autoLogin = {
          enable = true;
	  user = "patryk";
        };
      };

      desktopManager.plasma5.enable = true;
    };

    redshift = {
      enable = true;
      latitude = "50";
      longitude = "22";
      temperature.day = 3500;
      temperature.night = 2300;
    };
  };

  users.users.patryk = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" "adbusers" ];
     shell = pkgs.zsh;
  };

  system.stateVersion = "19.09";
}
