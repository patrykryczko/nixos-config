{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [

    # dev
    android-studio
    emacs
    openjdk
    neovim
    nodejs
    racket
    rustup
    watchman
    vscode
    yarn
    gradle

    # nodejs packages
    nodePackages.create-react-app
    nodePackages.react-native-cli
    
    #shell
    zsh
    zsh-autosuggestions
    zsh-syntax-highlighting
    oh-my-zsh

    # web
    firefox-bin
    tor-browser-bundle-bin
    chromium
    spotify

    # desktop
    anki
    akregator
    llpp
    libreoffice
    inkscape
    gimp
    qbittorrent
    mpv
    
    # utils
    ark
    git
    gwenview
    kdeconnect
    mc
    redshift
    spectacle
    subdl
    yakuake
    plasma-browser-integration
    tor
    unrar
    unzip
    wget
    yakuake
    youtube-dl
    zip
  ]; 

  nixpkgs.config = {
    allowUnfree = true;
  };

  fonts = {
    fontconfig.enable = true;
    fonts = with pkgs; [
      emojione
      noto-fonts
      noto-fonts-emoji
      fira
      fira-code
      fira-mono
    ];
  };
}
